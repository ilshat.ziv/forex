package com.example.forex.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "pair")
public class PairEntity {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "code")
    private final String mCode;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "is_favorite")
    private boolean isFavorite;

    public PairEntity(@NonNull String code) {
        mCode = code;
    }

    @Ignore
    public PairEntity(@NonNull String code, @NonNull String name) {
        this(code);
        mName = name;
    }

    @NotNull
    public String getCode() {
        return mCode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
