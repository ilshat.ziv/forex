package com.example.forex.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.forex.database.dao.PairDAO;
import com.example.forex.database.entity.PairEntity;

@Database(entities = {
        PairEntity.class,
    },
        version = 1,
        exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PairDAO getPairDAO();
}
