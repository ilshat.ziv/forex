package com.example.forex.database.dao;

import androidx.annotation.Nullable;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.forex.database.entity.PairEntity;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface PairDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addPairs(List<PairEntity> pairs);

    @Update
    void updatePair(PairEntity shop);

    @Query("SELECT * FROM pair")
    Single<List<PairEntity>> getPairs();

    @Query("SELECT * FROM pair WHERE name LIKE '%' || :search || '%'")
    Single<List<PairEntity>> getPairs(@Nullable String search);

    @Query("SELECT * FROM pair WHERE is_favorite = 1")
    Single<List<PairEntity>> getFavorite();
}
