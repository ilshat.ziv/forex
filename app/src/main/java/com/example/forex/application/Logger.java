package com.example.forex.application;

import androidx.annotation.NonNull;

import timber.log.Timber;

public class Logger {

    public static void debug(@NonNull String message) {
        Timber.d(message);
    }

    public static void exception(@NonNull Throwable throwable) {
        Timber.e(throwable);
    }
}
