package com.example.forex.application.snack;

public enum TypeMessage {
    Normal, Success, Info, Warning, Error
}