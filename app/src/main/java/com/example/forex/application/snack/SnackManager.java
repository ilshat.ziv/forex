package com.example.forex.application.snack;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class SnackManager implements ISnackManager {

    @ColorInt
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#FFFFFF");

    @ColorInt
    private static final int ERROR_COLOR = Color.parseColor("#FF4F61");

    @ColorInt
    private static final int INFO_COLOR = Color.parseColor("#3F51B5");

    @ColorInt
    private static final int SUCCESS_COLOR = Color.parseColor("#388E3C");

    @ColorInt
    private static final int WARNING_COLOR = Color.parseColor("#FFA900");

    private final View mRootView;
    private Snackbar mSnackbar;

    public SnackManager(@NonNull View view) {
        mRootView = view;
    }

    private Snackbar showNormal(@NonNull String message) {
        Snackbar snackbar;
        snackbar = Snackbar.make(mRootView, message, BaseTransientBottomBar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(DEFAULT_TEXT_COLOR);
        setTextStyle(view, Color.parseColor("#000000"));
        snackbar.show();
        return snackbar;
    }

    private Snackbar showSuccess(@NonNull String message) {
        Snackbar snackbar = Snackbar.make(mRootView, message, BaseTransientBottomBar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(SUCCESS_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showInfo(@NonNull String message) {
        Snackbar snackbar = Snackbar.make(mRootView, message, BaseTransientBottomBar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(INFO_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showWarning(@NonNull String message) {
        Snackbar snackbar = Snackbar.make(mRootView, message, BaseTransientBottomBar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(WARNING_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private Snackbar showError(@NonNull String message) {
        Snackbar snackbar = Snackbar.make(mRootView, message, BaseTransientBottomBar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(ERROR_COLOR);
        setTextStyle(view);
        snackbar.show();
        return snackbar;
    }

    private void setTextStyle(@NonNull View view) {
        setTextStyle(view, 0);
    }

    private void setTextStyle(@NonNull View view, int textColor) {
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);

        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        if (textColor != 0) {
            tv.setTextColor(textColor);
        }
    }

    @Override
    public void showLong(@NonNull TypeMessage type, @NonNull String message) {
        dismiss();

        switch (type) {
            case Normal: {
                mSnackbar = showNormal(message);
                break;
            }
            case Success: {
                mSnackbar = showSuccess(message);
                break;
            }
            case Info: {
                mSnackbar = showInfo(message);
                break;
            }
            case Warning: {
                mSnackbar = showWarning(message);
                break;
            }
            case Error: {
                mSnackbar = showError(message);
                break;
            }
        }
    }

    private void dismiss() {
        if (mSnackbar != null) {
            mSnackbar.dismiss();
        }
    }
}
