package com.example.forex.application;

import com.example.forex.database.DatabaseModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class AppDelegate extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        CommonModule commonModule = new CommonModule(this);
        return DaggerAppComponent.builder()
                .application(this)
                .context(commonModule)
                .database(new DatabaseModule())
                .build();
    }
}
