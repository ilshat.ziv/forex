package com.example.forex.application.snack;

import androidx.annotation.NonNull;

public interface ISnackManager {

    void showLong(@NonNull TypeMessage type, @NonNull String message);
}
