package com.example.forex.application;

import android.app.Application;

import com.example.forex.architecture.ViewModelModule;
import com.example.forex.database.DatabaseModule;
import com.example.forex.di.dagger.ActivityBindingModule;
import com.example.forex.repository.RepositoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        CommonModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBindingModule.class,
        ViewModelModule.class,
        RepositoryModule.class,
        DatabaseModule.class
})
public interface AppComponent extends AndroidInjector<AppDelegate> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        Builder context(CommonModule contextModule);

        Builder database(DatabaseModule databaseModule);

        AppComponent build();
    }
}