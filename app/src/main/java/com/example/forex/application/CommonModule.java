package com.example.forex.application;

import android.content.Context;

import androidx.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class CommonModule {

    private final Context mContext;

    CommonModule(@NonNull AppDelegate app) {
        mContext = app;
    }

    @Provides
    @Singleton
    Context getContext() {
        return mContext;
    }
}