package com.example.forex.socket.response;

import com.google.gson.annotations.SerializedName;

public abstract class BaseEventResponse {

    @SerializedName("event")
    private String mEvent;

    public void setEvent(String event) {
        mEvent = event;
    }

    public final String getEvent() {
        return mEvent;
    }
}
