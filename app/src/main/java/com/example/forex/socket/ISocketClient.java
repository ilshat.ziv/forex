package com.example.forex.socket;

import androidx.annotation.NonNull;

import com.example.forex.socket.request.BaseEventRequest;

public interface ISocketClient {

    void connect();

    void disconnect();

    void invoke(@NonNull BaseEventRequest event);

    void setOnSocketClientListener(OnSocketClientListener listener);

    enum ForexEvent {
        INFO,
        SUBSCRIBE,
        SUBSCRIBED,
        UNSUBSCRIBE,
    }

    enum ForexChannel {
        TICKER
    }
}
