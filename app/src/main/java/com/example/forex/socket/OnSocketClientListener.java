package com.example.forex.socket;

import androidx.annotation.NonNull;

import com.example.forex.socket.response.BaseEventResponse;

public interface OnSocketClientListener {

    void onConnect();

    void onDisconnect();

    void onMessage(String message);

    void onError(Throwable throwable);

    void onMessageReceived(@NonNull BaseEventResponse response);

}
