package com.example.forex.socket.request;

import androidx.annotation.NonNull;

import com.example.forex.socket.ISocketClient;
import com.google.gson.annotations.SerializedName;

class UnSubscribeRequest extends BaseEventRequest {

    @SerializedName("channel")
    private final String mChannel;

    UnSubscribeRequest(@NonNull ISocketClient.ForexChannel channel) {
        super(ISocketClient.ForexEvent.UNSUBSCRIBE);
        mChannel = channel.name().toLowerCase();
    }
}