package com.example.forex.socket.request;

import androidx.annotation.NonNull;

import com.example.forex.socket.ISocketClient;
import com.google.gson.annotations.SerializedName;

public abstract class BaseEventRequest {

    @SerializedName("event")
    private final String mEvent;

    BaseEventRequest(@NonNull ISocketClient.ForexEvent event) {
        mEvent = event.name().toLowerCase();
    }
}
