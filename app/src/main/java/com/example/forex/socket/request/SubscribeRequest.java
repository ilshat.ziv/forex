package com.example.forex.socket.request;

import androidx.annotation.NonNull;

import com.example.forex.socket.ISocketClient;
import com.google.gson.annotations.SerializedName;

class SubscribeRequest extends BaseEventRequest {

    @SerializedName("channel")
    private final String mChannel;

    SubscribeRequest(@NonNull ISocketClient.ForexChannel channel) {
        super(ISocketClient.ForexEvent.SUBSCRIBE);
        mChannel = channel.name().toLowerCase();
    }
}