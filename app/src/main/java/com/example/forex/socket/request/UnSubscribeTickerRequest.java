package com.example.forex.socket.request;

import androidx.annotation.NonNull;

import com.example.forex.socket.ISocketClient;
import com.google.gson.annotations.SerializedName;

public class UnSubscribeTickerRequest extends UnSubscribeRequest {

    @SerializedName("pair")
    private final String mPair;

    public UnSubscribeTickerRequest(@NonNull String pair) {
        super(ISocketClient.ForexChannel.TICKER);
        mPair = pair;
    }
}