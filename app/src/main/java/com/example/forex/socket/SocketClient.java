package com.example.forex.socket;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.forex.application.Logger;
import com.example.forex.socket.request.BaseEventRequest;
import com.example.forex.socket.response.BaseEventResponse;
import com.example.forex.socket.response.UndefEventResponse;
import com.example.forex.socket.response.event.StatusEventResponse;
import com.example.forex.socket.response.ticker.TickerChangeEventResponse;
import com.example.forex.utils.CollectionUtils;
import com.google.gson.Gson;

import com.example.socket.RxWebSocket;
import com.example.socket.entities.SocketMessageEvent;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SocketClient implements ISocketClient {

    private final CompositeDisposable mCompositeDisposable;
    private final RxWebSocket mRxWebSocket;
    private SocketState mSocketState;
    private OnSocketClientListener mOnSocketClientListener;

    public SocketClient(@NonNull String url) {
        mCompositeDisposable = new CompositeDisposable();
        mRxWebSocket = new RxWebSocket(url);
        initListener();
    }

    private void initListener() {
        Disposable disposable = mRxWebSocket.onOpen()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onOpenEvent -> {
                    mSocketState = SocketState.OPENED;
                    if (mOnSocketClientListener != null) {
                        mOnSocketClientListener.onConnect();
                    }
                }, this::onError);
        mCompositeDisposable.add(disposable);

        disposable = mRxWebSocket.onClosed()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(socketClosedEvent -> {
                    mSocketState = SocketState.CLOSED;
                    if (mOnSocketClientListener != null) {
                        mOnSocketClientListener.onDisconnect();
                    }
                }, this::onError);
        mCompositeDisposable.add(disposable);

        disposable = mRxWebSocket.onClosing()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(socketClosingEvent -> mSocketState = SocketState.CLOSING, this::onError);
        mCompositeDisposable.add(disposable);

        disposable = mRxWebSocket.onTextMessage()
                .map(this::processEventData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(socketMessageEvent -> {
                    if (mOnSocketClientListener != null) {
                        mOnSocketClientListener.onMessage(socketMessageEvent.toString());
                        mOnSocketClientListener.onMessageReceived(socketMessageEvent);
                    }
                }, this::onError);
        mCompositeDisposable.add(disposable);

        disposable = mRxWebSocket.onBinaryMessage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(socketClosingEvent -> {
                    if (mOnSocketClientListener != null) {
                        mOnSocketClientListener.onMessage(socketClosingEvent.getText());
                    }
                }, this::onError);
        mCompositeDisposable.add(disposable);

        disposable = mRxWebSocket.onFailure()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        socketFailureEvent -> onError(socketFailureEvent.getException())
                        , this::onError);
        mCompositeDisposable.add(disposable);
    }

    private void onError(Throwable throwable) {
        Logger.exception(throwable);
        if (mOnSocketClientListener != null) {
            mOnSocketClientListener.onError(throwable);
        }
    }

    @Override
    public void connect() {
        if (mSocketState == SocketState.OPENED || mSocketState == SocketState.OPENING) {
            return;
        }
        mSocketState = SocketState.OPENING;
        mRxWebSocket.connect();
    }

    @Override
    public void disconnect() {
        if (mSocketState == SocketState.CLOSED || mSocketState == SocketState.CLOSING) {
            return;
        }
        Disposable disposable = mRxWebSocket.close()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> mSocketState = SocketState.CLOSED, this::onError);

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void invoke(@NonNull BaseEventRequest event) {
        Disposable disposable = mRxWebSocket.sendMessage(new Gson(), event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(b -> Logger.debug(String.valueOf(b)), this::onError);
        mCompositeDisposable.add(disposable);
    }

    private BaseEventResponse processEventData(@NonNull SocketMessageEvent event) {
        Logger.debug(String.valueOf(event));

        String text = event.getText();
        text = TextUtils.isEmpty(text) ? "" : text;
        Object o = new Gson().fromJson(text, Object.class);
        if (o instanceof LinkedTreeMap) {
            String type = (String) ((LinkedTreeMap) o).get("event");
            StatusEventResponse response = new StatusEventResponse();
            response.setEvent(type);
            return response;
        } else if (o instanceof ArrayList) {
            ArrayList o1 = (ArrayList) o;
            if (CollectionUtils.size(o1) == 11)
                return new TickerChangeEventResponse(o1);
        }

        return new UndefEventResponse();
    }

    @Override
    public void setOnSocketClientListener(OnSocketClientListener listener) {
        mOnSocketClientListener = listener;
    }

    public enum SocketState {
        CLOSED,
        OPENING,
        OPENED,
        CLOSING
    }
}