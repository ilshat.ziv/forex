package com.example.forex.socket.response.ticker;

import androidx.annotation.NonNull;

import com.example.forex.socket.response.event.StatusEventResponse;

import java.util.ArrayList;

public final class TickerChangeEventResponse extends StatusEventResponse {

    private final double mBid;
    private final double mAsk;
    private final double mHigh;
    private final double mLow;

    /**
     * [92282,7655.4,48.41706348,7655.5,21.388572710000002,-26.5,-0.0034,7655.5,7142.22166567,7771.5,7473.5]
     *
     * CHANNEL_ID	integer	Channel ID
     * BID	float	Price of last highest bid
     * BID_SIZE	float	Size of the last highest bid
     * ASK	float	Price of last lowest ask
     * ASK_SIZE	float	Size of the last lowest ask
     * DAILY_CHANGE	float	Amount that the last price has changed since yesterday
     * DAILY_CHANGE_PERC	float	Amount that the price has changed expressed in percentage terms
     * LAST_PRICE	float	Price of the last trade.
     * VOLUME	float	Daily volume
     * HIGH	float	Daily high
     * LOW	float	Daily low
     *
     * @param data - массив данных тика
     */
    public TickerChangeEventResponse(@NonNull ArrayList data) {
        mBid = (double) data.get(1);
        mAsk = (double) data.get(3);
        mHigh = (double) data.get(9);
        mLow = (double) data.get(10);
    }

    public float getBid() {
        return (float) mBid;
    }

    public float getAsk() {
        return (float) mAsk;
    }

    public float getHigh() {
        return (float) mHigh;
    }

    public float getLow() {
        return (float) mLow;
    }
}
