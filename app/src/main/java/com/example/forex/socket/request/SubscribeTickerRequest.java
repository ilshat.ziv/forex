package com.example.forex.socket.request;

import androidx.annotation.NonNull;

import com.example.forex.socket.ISocketClient;

import com.google.gson.annotations.SerializedName;

public class SubscribeTickerRequest extends SubscribeRequest {

    @SerializedName("pair")
    private final String mPair;

    public SubscribeTickerRequest(@NonNull String pair) {
        super(ISocketClient.ForexChannel.TICKER);
        mPair = pair;
    }
}