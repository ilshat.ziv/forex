package com.example.forex.rx;

import androidx.appcompat.widget.SearchView;

import com.example.forex.ui.widget.search.BaseSearchView;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

final class SearchViewQueryTextChangesOnSubscribe implements ObservableOnSubscribe<SearchViewQueryTextEvent> {

    private final BaseSearchView mView;

    SearchViewQueryTextChangesOnSubscribe(BaseSearchView view) {
        this.mView = view;
    }

    @Override
    public void subscribe(ObservableEmitter<SearchViewQueryTextEvent> emitter) {
        SearchView.OnQueryTextListener watcher = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!emitter.isDisposed()) {
                    emitter.onNext(SearchViewQueryTextEvent.create(mView, query, false));
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!emitter.isDisposed()) {
                    emitter.onNext(SearchViewQueryTextEvent.create(mView, mView.getQuery(), true));
                    return true;
                }
                return false;
            }
        };
        mView.setOnQueryTextListener(watcher);

        emitter.onNext(SearchViewQueryTextEvent.create(mView, mView.getQuery(), false));
    }
}
