package com.example.forex.rx;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

import com.example.forex.ui.widget.search.BaseSearchView;

import java.util.Objects;

public final class SearchViewQueryTextEvent extends ViewEvent<BaseSearchView> {

    @CheckResult
    @NonNull
    public static SearchViewQueryTextEvent create(@NonNull BaseSearchView view,
                                                  @NonNull CharSequence queryText, boolean submitted) {
        return new SearchViewQueryTextEvent(view, queryText, submitted);
    }

    private final CharSequence mQueryText;
    private final boolean mSubmitted;

    private SearchViewQueryTextEvent(@NonNull BaseSearchView view, @NonNull CharSequence queryText,
                                     boolean submitted) {
        mQueryText = queryText;
        mSubmitted = submitted;
    }

    @NonNull
    public CharSequence queryText() {
        return mQueryText;
    }

    public boolean isSubmitted() {
        return mSubmitted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchViewQueryTextEvent that = (SearchViewQueryTextEvent) o;
        return mSubmitted == that.mSubmitted &&
                mQueryText.equals(that.mQueryText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mQueryText, mSubmitted);
    }
}
