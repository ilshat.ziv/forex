package com.example.forex.rx;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

import com.example.forex.ui.widget.search.BaseSearchView;

import io.reactivex.Observable;

import static androidx.core.util.Preconditions.checkNotNull;

public class RxSearchView {

    @CheckResult
    @NonNull
    public static Observable<SearchViewQueryTextEvent> queryTextChangeEvents(@NonNull BaseSearchView view) {
        checkNotNull(view,"view == null");
        return Observable.create(new SearchViewQueryTextChangesOnSubscribe(view));
    }
}
