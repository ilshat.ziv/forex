package com.example.forex.repository;

import android.content.Context;

import com.example.forex.database.AppDatabase;
import com.example.forex.repository.currency.ForexRepository;
import com.example.forex.repository.currency.IForexRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    IForexRepository provideIExchangeRepository(Context context, AppDatabase appDatabase) {
        return new ForexRepository(context, appDatabase.getPairDAO());
    }
}
