package com.example.forex.repository.currency;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.forex.database.dao.PairDAO;
import com.example.forex.database.entity.PairEntity;
import com.example.forex.ui.fragment.pair.model.PairModel;
import com.example.forex.utils.CollectionUtils;
import com.example.forex.utils.FileUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import io.reactivex.Single;

public class ForexRepository implements IForexRepository {

    private Context mContext;
    private PairDAO mPairDAO;

    public ForexRepository(Context context, PairDAO pairDAO) {
        mContext = context;
        mPairDAO = pairDAO;
    }

    @Override
    public Single<List<PairModel>> getPairs(String name) {
        Single<List<PairEntity>> s1 = TextUtils.isEmpty(name)
                ? mPairDAO.getPairs()
                : mPairDAO.getPairs(name);
        Single<List<PairEntity>> s2 = getFromRemotePairs(name);

        return Single.concat(s1, s2)
                .filter(countries -> !CollectionUtils.isEmpty(countries))
                .firstOrError()
                .toObservable()
                .flatMapIterable(list -> list)
                .map(PairModel::new)
                .toList()
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof NoSuchElementException) {
                        return Single.just(new ArrayList<>());
                    }
                    return Single.error(throwable);
                });
    }

    private Single<List<PairEntity>> getFromRemotePairs(String name) {

        Single<List<PairEntity>> s1 = Single.just("symbols.json")
                .map(n -> FileUtils.loadJsonFromAssets(mContext, n))
                .map(json -> {
                    List<PairEntity> pairs = new ArrayList<>();

                    Gson gson = new Gson();
                    List list = (ArrayList) gson.fromJson(json, Object.class);
                    for (Object o : list) {
                        String code = String.valueOf(o);
                        pairs.add(new PairEntity(code, code));
                    }

                    return pairs;
                });

        if (!TextUtils.isEmpty(name)) {
            return s1.toObservable()
                    .flatMapIterable(list -> list)
                    .filter(p -> p.getName().contains(name))
                    .toList();
        } else return s1.doOnSuccess(l -> mPairDAO.addPairs(l));
    }

    @Override
    public Single<List<PairModel>> getFavoritePairs() {
        return mPairDAO.getFavorite()
                .toObservable()
                .flatMapIterable(list -> list)
                .map(PairModel::new)
                .toList();
    }

    @Override
    public Single<PairModel> addFavorite(@NonNull PairModel model) {
        return Single.just(model)
                .map(m -> {
                    PairEntity pair = new PairEntity(m.getCode(), m.getName());
                    pair.setFavorite(true);
                    mPairDAO.updatePair(pair);

                    m.setFavorite(true);
                    return m;
                });
    }

    @Override
    public Single<PairModel> deleteFavorite(@NonNull PairModel model) {
        return Single.just(model)
                .map(m -> {
                    PairEntity pair = new PairEntity(m.getCode(), m.getName());
                    pair.setFavorite(false);
                    mPairDAO.updatePair(pair);

                    m.setFavorite(false);
                    return m;
                });
    }
}
