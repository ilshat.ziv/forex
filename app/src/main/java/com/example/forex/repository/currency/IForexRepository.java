package com.example.forex.repository.currency;

import androidx.annotation.NonNull;

import com.example.forex.ui.fragment.pair.model.PairModel;

import java.util.List;

import io.reactivex.Single;

public interface IForexRepository {

    Single<List<PairModel>> getPairs(String name);

    Single<List<PairModel>> getFavoritePairs();

    Single<PairModel> addFavorite(@NonNull PairModel model);

    Single<PairModel> deleteFavorite(@NonNull PairModel model);
}