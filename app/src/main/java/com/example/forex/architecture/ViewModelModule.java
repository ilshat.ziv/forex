package com.example.forex.architecture;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.forex.di.dagger.ViewModelKey;
import com.example.forex.ui.fragment.chart.ChartViewModel;
import com.example.forex.ui.fragment.pair.PairListViewModel;
import com.example.forex.ui.fragment.favorite.FavoritePairListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PairListViewModel.class)
    ViewModel bindDateListViewModel(PairListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FavoritePairListViewModel.class)
    ViewModel bindFavoritePairListViewModel(FavoritePairListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ChartViewModel.class)
    ViewModel bindChartViewModel(ChartViewModel viewModel);

    @Binds
    ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
