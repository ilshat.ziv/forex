package com.example.forex.architecture;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.forex.application.Logger;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseViewModel extends ViewModel {

    protected CompositeDisposable mCompositeDisposable;

    private final ObservableField<Boolean> isShowProgressView = new ObservableField<>();
    protected final MutableLiveData<Boolean> isShowProgressLiveData;

    private final ObservableField<Throwable> mError = new ObservableField<>();
    private final MutableLiveData<Throwable> mErrorLiveData;

    private final ObservableField<String> mSuccessMessage = new ObservableField<>();
    private final MutableLiveData<String> mSuccessMessageLiveData;

    protected BaseViewModel() {
        mCompositeDisposable = new CompositeDisposable();
        this.isShowProgressLiveData = new MutableLiveData<>();
        this.isShowProgressLiveData.setValue(true);
        mErrorLiveData = new MutableLiveData<>();
        mSuccessMessageLiveData = new MutableLiveData<>();
    }

    protected void showError(Throwable throwable) {
        this.isShowProgressLiveData.setValue(false);
        Logger.exception(throwable);
        mErrorLiveData.setValue(throwable);
    }

    public MutableLiveData<Boolean> getIsShowProgressLiveData() {
        return isShowProgressLiveData;
    }

    public MutableLiveData<Throwable> getErrorLiveData() {
        return mErrorLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        this.isShowProgressLiveData.observe(owner, isShowProgressView::set);
        mErrorLiveData.observe(owner, mError::set);
        mSuccessMessageLiveData.observe(owner, mSuccessMessage::set);
    }
}