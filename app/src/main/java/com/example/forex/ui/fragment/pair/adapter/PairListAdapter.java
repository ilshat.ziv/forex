package com.example.forex.ui.fragment.pair.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.example.forex.R;
import com.example.forex.databinding.ItemPairListBinding;
import com.example.forex.ui.binding.BaseRecyclerAdapter;
import com.example.forex.ui.fragment.pair.model.PairModel;

public class PairListAdapter extends BaseRecyclerAdapter<PairModel, BaseRecyclerAdapter.ViewHolder> {

    private final OnPairListListener mOnPairListListener;

    public PairListAdapter(@NonNull OnPairListListener listener) {
        super(R.layout.item_pair_list);
        mOnPairListListener = listener;
    }

    @Override
    protected ViewHolder buildViewHolder(ViewDataBinding binding) {
        return new ViewHolder(binding);
    }

    class ViewHolder extends BaseRecyclerAdapter.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding);
            ((ItemPairListBinding) binding).setListener(mOnPairListListener);
        }
    }

    public interface OnPairListListener {
        void onCurrencyClick(@NonNull PairModel model);
        void onFavoritePairClick(@NonNull PairModel model);
    }
}
