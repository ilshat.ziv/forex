package com.example.forex.ui.widget.toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.forex.R;

public class ToolbarController implements IToolbarController {

    private final AppCompatActivity mAppCompatActivity;

    public ToolbarController(@NonNull AppCompatActivity fragmentActivity) {
        mAppCompatActivity = fragmentActivity;
        Toolbar toolbar = fragmentActivity.findViewById(R.id.toolbar);
        if (toolbar != null) {
            fragmentActivity.setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(view -> fragmentActivity.onBackPressed());
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        }
    }

    @Override
    public void setTitle(int title) {
        String text = mAppCompatActivity.getResources().getString(title);
        setTitle(text);
    }

    @Override
    public void setTitle(@NonNull String title) {
        ActionBar actionBar = mAppCompatActivity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }
}