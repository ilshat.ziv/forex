package com.example.forex.ui.widget.refresh;

import android.content.Context;
import android.util.AttributeSet;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.forex.R;

public class RefreshView extends SwipeRefreshLayout {

    public RefreshView(Context context) {
        super(context);
        setColorSchemeResources(R.color.colorAccent);
    }

    public RefreshView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorSchemeResources(R.color.colorAccent);
    }

    public void hide() {
        post(() -> setRefreshing(false));
    }

    public void show() {
        post(() -> setRefreshing(true));
    }
}