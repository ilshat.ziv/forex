package com.example.forex.ui.fragment.chart.model;

import androidx.annotation.NonNull;

import com.example.forex.socket.response.ticker.TickerChangeEventResponse;

import java.util.Objects;

public class CandleModel {

    private final float mHigh;
    private final float mLow;
    private final float mClose;
    private final float mOpen;

    private final String mPair;

    public CandleModel(@NonNull String pair, @NonNull TickerChangeEventResponse data) {
        mPair = pair;

        mHigh = data.getHigh();
        mLow = data.getLow();
        mOpen = data.getAsk();
        mClose = data.getBid();
    }

    public String getPair() {
        return mPair;
    }

    public float getClose() {
        return mClose;
    }

    public float getOpen() {
        return mOpen;
    }

    public float getHigh() {
        return mHigh;
    }

    public float getLow() {
        return mLow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CandleModel that = (CandleModel) o;
        return Float.compare(that.mHigh, mHigh) == 0 &&
                Float.compare(that.mLow, mLow) == 0 &&
                Float.compare(that.mClose, mClose) == 0 &&
                Float.compare(that.mOpen, mOpen) == 0 &&
                Objects.equals(mPair, that.mPair);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mHigh, mLow, mClose, mOpen, mPair);
    }
}
