package com.example.forex.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.forex.BR;
import com.example.forex.application.snack.ISnackManager;
import com.example.forex.application.snack.SnackManager;
import com.example.forex.application.snack.TypeMessage;
import com.example.forex.architecture.BaseViewModel;
import com.example.forex.architecture.ViewModelFactory;

import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends DaggerFragment {

    private ISnackManager mISnackManager;

    /* Observers */

    private final Observer<Boolean> mProgressProcess = isShow -> {
        if (isShow != null && isShow) {
            onShowProgressView();
        } else {
            onHideProgressView();
        }
    };

    private final Observer<Throwable> mErrorProcess = throwable -> {
        if (throwable == null) {
            return;
        }
        showErrorMessage(String.valueOf(throwable.getMessage()));
        getViewModel().getErrorLiveData().setValue(null);
    };

    /* DI */

    @Inject
    ViewModelFactory mViewModelFactory;

    /* Binding */

    private T mViewDataBinding;
    private V mViewModel;

    @LayoutRes
    protected int getLayoutId() {
        return 0;
    }

    protected V getViewModel() {
        return mViewModel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    protected final void initViewModel() {
        initViewModel(mViewModelFactory, this);
    }

    private void initViewModel(@NonNull ViewModelFactory viewModelFactory, Fragment fragment) {
        ParameterizedType supperClazz = (ParameterizedType) getClass().getGenericSuperclass();
        Class<V> clazz = (Class<V>) supperClazz.getActualTypeArguments()[1];
        mViewModel = ViewModelProviders.of(fragment, viewModelFactory).get(clazz);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(BR.modelView, mViewModel);
        mViewDataBinding.executePendingBindings();
        mISnackManager = new SnackManager(view);

        mViewModel.getIsShowProgressLiveData().observe(getViewLifecycleOwner(), mProgressProcess);
        onBindError();
    }

    private void onBindError() {
        mViewModel.getErrorLiveData().observe(getViewLifecycleOwner(), mErrorProcess);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return mViewDataBinding.getRoot();
    }

    protected void onShowProgressView() {
    }

    protected void onHideProgressView() {
    }

    private void showErrorMessage(@NonNull String message) {
        if (isAdded()) {
            mISnackManager.showLong(TypeMessage.Error, message);
        }
    }
}