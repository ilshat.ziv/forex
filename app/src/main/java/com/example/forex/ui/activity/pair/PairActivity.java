package com.example.forex.ui.activity.pair;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.forex.R;
import com.example.forex.ui.activity.BaseActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashSet;
import java.util.Set;

public class PairActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_pair);
        setupUi();
    }

    private void setupUi() {
        BottomNavigationView bottomNavigationBar = findViewById(R.id.bottomNavigationBar);

        Set<Integer> topActions = new HashSet<>();
        topActions.add(R.id.fr_pair_list);
        topActions.add(R.id.fr_favorite_pair_list);

        AppBarConfiguration.Builder builder = new AppBarConfiguration.Builder(topActions);
        AppBarConfiguration configuration = builder.build();

        NavigationUI.setupWithNavController(
                findViewById(R.id.toolbar),
                mNavController,
                configuration
        );
        NavigationUI.setupWithNavController(bottomNavigationBar, mNavController);
        bottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            if (!item.isChecked()) {
                int itemId = item.getItemId();
                if (!mNavController.popBackStack(itemId, false)) {
                    mNavController.navigate(itemId);
                }
                return true;
            }
            return false;
        });
    }
}
