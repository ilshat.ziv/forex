package com.example.forex.ui.fragment.favorite;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.forex.R;
import com.example.forex.databinding.FrFavoritePairListBinding;
import com.example.forex.ui.activity.BaseActivity;
import com.example.forex.ui.activity.chart.ChartActivity;
import com.example.forex.ui.fragment.BaseFragment;
import com.example.forex.ui.fragment.pair.model.PairModel;
import com.example.forex.ui.fragment.favorite.adapter.FavoritePairListAdapter;
import com.example.forex.ui.widget.refresh.RefreshView;
import com.example.forex.ui.widget.toolbar.IToolbarController;

public class FavoritePairListFragment extends BaseFragment<FrFavoritePairListBinding, FavoritePairListViewModel> {

    private RefreshView mRefreshView;
    private FavoritePairListAdapter mFavoritePairListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fr_favorite_pair_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
        mRefreshView = requireView().findViewById(R.id.swipeRefreshView);
        mRefreshView.setEnabled(false);
        initDateList();
    }

    private void initToolbar() {
        BaseActivity activity = (BaseActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(R.string.app_favorite_pair_list_title);
    }

    private void initDateList() {
        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        DividerItemDecoration itemDecor = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.item_decor));
        recyclerView.addItemDecoration(itemDecor);
        mFavoritePairListAdapter = new FavoritePairListAdapter(new FavoritePairListAdapter.OnPairListListener() {
            @Override
            public void onCurrencyClick(@NonNull PairModel model) {
                NavController navController = Navigation.findNavController(requireView());
                Bundle bundle = new Bundle();
                bundle.putString(ChartActivity.PAIR, model.getCode());
                navController.navigate(R.id.ac_chart, bundle);
            }

            @Override
            public void onFavoritePairClick(@NonNull PairModel model) {
                getViewModel().deleteFavorite(model);
            }
        });
        recyclerView.setAdapter(mFavoritePairListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bind();
    }

    private void bind() {
        getViewModel().subscribeToLiveData(this);
        getViewModel().getCurrencyPairListLiveData().observe(getViewLifecycleOwner(),
                list -> mFavoritePairListAdapter.replaceDataSet(list)
        );
        getViewModel().bindData();
    }

    @Override
    protected void onShowProgressView() {
        mRefreshView.show();
    }

    @Override
    protected void onHideProgressView() {
        mRefreshView.hide();
    }
}