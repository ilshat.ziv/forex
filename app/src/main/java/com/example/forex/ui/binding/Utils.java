package com.example.forex.ui.binding;

import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

import com.example.forex.R;

/**
 * Binding утилиты
 */
public class Utils {


    /**
     * Метод загрузки изображения
     *
     * @param view         - ImageView.class extend
     * @param imageUrl     - id ресурс загружаемого изображения
     * @param defaultImage - дефолтное изображение, если загрузка не удалась
     */
    @BindingAdapter({"imageUrl", "defaultImage"})
    public static void loadImage(@NonNull ImageView view,
                                 int imageUrl,
                                 @DrawableRes int defaultImage) {

        defaultImage = (defaultImage == 0) ? R.drawable.ic_launcher_foreground : defaultImage;

        view.setImageResource((imageUrl > 0) ? imageUrl : defaultImage);
    }
}
