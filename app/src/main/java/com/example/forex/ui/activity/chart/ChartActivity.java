package com.example.forex.ui.activity.chart;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.forex.R;
import com.example.forex.ui.activity.BaseActivity;

public class ChartActivity extends BaseActivity {

    public static final String PAIR = "PAIR";
    public static final String DEFAULT_PAIR = "BTCUSD";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_chart);
    }
}
