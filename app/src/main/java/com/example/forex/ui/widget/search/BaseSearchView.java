package com.example.forex.ui.widget.search;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.SearchView;

public class BaseSearchView extends SearchView {

    private String mTextQuery;

    private boolean isCollapsed;
    private boolean isExpanded;

    private SearchView.OnQueryTextListener mOnQueryTextListener;
    private OnCloseListener mOnCloseListener;

    public BaseSearchView(Context context) {
        super(context);
    }

    public BaseSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOnCloseListener(OnCloseListener listener) {
        mOnCloseListener = listener;
        super.setOnCloseListener(listener);
    }

    @Override
    public void setOnQueryTextListener(OnQueryTextListener listener) {
        mOnQueryTextListener = listener;
        super.setOnQueryTextListener(listener);
    }

    @Override
    public void onActionViewCollapsed() {
        isCollapsed = true;
        mTextQuery = getQuery().toString();
        super.onActionViewCollapsed();
        if (mOnCloseListener != null) {
            mOnCloseListener.onClose();
        }
    }

    @Override
    public void onActionViewExpanded() {
        super.setOnQueryTextListener(null);
        super.onActionViewExpanded();
        if (mTextQuery != null) {
            setQuery(mTextQuery, false);
        }
        super.setOnQueryTextListener(mOnQueryTextListener);
    }

    @Override
    public void setQuery(CharSequence query, boolean submit) {
        if (isCollapsed) {
            isCollapsed = false;
            return;
        }
        if (isExpanded) {
            isExpanded = false;
            return;
        }
        super.setQuery(query, submit);
    }

    public void setRestoreQuery(String restoreQuery) {
        if (restoreQuery != null) {
            mTextQuery = restoreQuery;
        }
    }
}
