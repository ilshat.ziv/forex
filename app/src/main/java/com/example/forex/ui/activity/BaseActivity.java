package com.example.forex.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.forex.R;
import com.example.forex.ui.widget.toolbar.IToolbarController;
import com.example.forex.ui.widget.toolbar.ToolbarController;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    private IToolbarController mIToolbarController;
    protected NavController mNavController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initNavigation();
        initToolbar();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initNavigation();
        initToolbar();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        initNavigation();
        initToolbar();
    }

    private void initNavigation() {
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        if (navHostFragment != null) {
            mNavController = navHostFragment.getNavController();
        }
    }

    private void initToolbar() {
        mIToolbarController = new ToolbarController(this);
    }

    public IToolbarController getIToolbarController() {
        return mIToolbarController;
    }
}
