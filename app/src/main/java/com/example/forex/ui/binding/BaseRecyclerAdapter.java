package com.example.forex.ui.binding;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.example.forex.BR;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<I, VH extends BaseRecyclerAdapter.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    @LayoutRes
    private final int mItemViewLayout;

    private List<I> mItems = new ArrayList<>();

    protected BaseRecyclerAdapter(int itemViewLayout) {
        mItemViewLayout = itemViewLayout;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, mItemViewLayout, parent, false);
        return buildViewHolder(binding);
    }

    protected VH buildViewHolder(ViewDataBinding binding) {
        return (VH) new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onBind(getItem(position));
    }

    private I getItem(int position){
        return mItems.get(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void replaceDataSet(List<I> list) {
        mItems = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void onBind(Object data) {
            this.binding.setVariable(BR.modelView, data);
            this.binding.executePendingBindings();
        }
    }
}
