package com.example.forex.ui.fragment.favorite;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.forex.architecture.BaseViewModel;
import com.example.forex.repository.currency.IForexRepository;
import com.example.forex.ui.fragment.pair.model.PairModel;
import com.example.forex.utils.CollectionUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class FavoritePairListViewModel extends BaseViewModel {

    private final IForexRepository mIForexRepository;
    private DisposableSingleObserver mDeleteDisposableSingleObserver;
    private DisposableSingleObserver mDisposableSingleObserver;

    private final MutableLiveData<List<PairModel>> mCurrencyPairListViewModel = new MutableLiveData<>();

    @Inject
    FavoritePairListViewModel(IForexRepository iForexRepository) {
        super();
        mIForexRepository = iForexRepository;
    }

    @Override
    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        super.subscribeToLiveData(owner);
        mCurrencyPairListViewModel.observe(owner, currencyPairList -> isShowProgressLiveData.setValue(false));
    }

    MutableLiveData<List<PairModel>> getCurrencyPairListLiveData() {
        return mCurrencyPairListViewModel;
    }

    void bindData() {
        if (mDisposableSingleObserver != null && !mDisposableSingleObserver.isDisposed()) return;

        isShowProgressLiveData.setValue(true);

        mDisposableSingleObserver = mIForexRepository.getFavoritePairs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<PairModel>>() {
                    @Override
                    public void onSuccess(List<PairModel> data) {
                        mDisposableSingleObserver.dispose();
                        mCurrencyPairListViewModel.setValue(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDisposableSingleObserver);
    }

    void deleteFavorite(@NonNull PairModel model) {
        if (mDeleteDisposableSingleObserver != null && !mDeleteDisposableSingleObserver.isDisposed()) return;

        isShowProgressLiveData.setValue(true);

        mDeleteDisposableSingleObserver = mIForexRepository.deleteFavorite(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PairModel>() {
                    @Override
                    public void onSuccess(PairModel data) {
                        mDeleteDisposableSingleObserver.dispose();

                        List<PairModel> value = mCurrencyPairListViewModel.getValue();
                        if (!CollectionUtils.isEmpty(value)) {
                            value.remove(data);
                        }
                        mCurrencyPairListViewModel.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDeleteDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDeleteDisposableSingleObserver);
    }
}
