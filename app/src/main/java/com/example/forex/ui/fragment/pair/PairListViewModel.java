package com.example.forex.ui.fragment.pair;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.forex.application.Logger;
import com.example.forex.architecture.BaseViewModel;
import com.example.forex.repository.currency.IForexRepository;
import com.example.forex.ui.fragment.pair.model.PairModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.Observable;

public class PairListViewModel extends BaseViewModel {

    private final IForexRepository mIForexRepository;

    private DisposableSingleObserver mDisposableSingleObserver;
    private DisposableSingleObserver mAddDisposableSingleObserver;
    private DisposableSingleObserver mDeleteDisposableSingleObserver;

    private final MutableLiveData<String> mQueryStringLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<PairModel>> mCurrencyPairListViewModel = new MutableLiveData<>();

    @Inject
    PairListViewModel(IForexRepository iForexRepository) {
        super();
        mIForexRepository = iForexRepository;
        mQueryStringLiveData.setValue("");
    }

    @Override
    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        super.subscribeToLiveData(owner);
        mCurrencyPairListViewModel.observe(owner, currencyPairList -> isShowProgressLiveData.setValue(false));
        mQueryStringLiveData.observe(owner, this::bindData);
    }

    MutableLiveData<List<PairModel>> getCurrencyPairListLiveData() {
        return mCurrencyPairListViewModel;
    }

    private void bindData(String name) {
        if (mDisposableSingleObserver != null && !mDisposableSingleObserver.isDisposed()) return;

        isShowProgressLiveData.setValue(true);

        mDisposableSingleObserver = mIForexRepository.getPairs(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<PairModel>>() {
                    @Override
                    public void onSuccess(List<PairModel> data) {
                        mDisposableSingleObserver.dispose();
                        mCurrencyPairListViewModel.setValue(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDisposableSingleObserver);
    }

    void addFavorite(@NonNull PairModel model) {
        if (mAddDisposableSingleObserver != null && !mAddDisposableSingleObserver.isDisposed())
            return;

        isShowProgressLiveData.setValue(true);

        mAddDisposableSingleObserver = mIForexRepository.addFavorite(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PairModel>() {
                    @Override
                    public void onSuccess(PairModel data) {
                        mAddDisposableSingleObserver.dispose();
                        List<PairModel> value = mCurrencyPairListViewModel.getValue();
                        mCurrencyPairListViewModel.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mAddDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mAddDisposableSingleObserver);
    }

    void deleteFavorite(@NonNull PairModel model) {
        if (mDeleteDisposableSingleObserver != null && !mDeleteDisposableSingleObserver.isDisposed())
            return;

        isShowProgressLiveData.setValue(true);

        mDeleteDisposableSingleObserver = mIForexRepository.deleteFavorite(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PairModel>() {
                    @Override
                    public void onSuccess(PairModel data) {
                        mDeleteDisposableSingleObserver.dispose();

                        List<PairModel> value = mCurrencyPairListViewModel.getValue();
                        mCurrencyPairListViewModel.setValue(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDeleteDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDeleteDisposableSingleObserver);
    }

    void bindSearchView(@NonNull Observable<String> obs) {
        Disposable disposable = obs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mQueryStringLiveData::setValue, Logger::exception);

        mCompositeDisposable.add(disposable);
    }
}
