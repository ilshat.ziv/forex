package com.example.forex.ui.fragment.pair.model;

import androidx.annotation.NonNull;

import com.example.forex.database.entity.PairEntity;

import java.util.Objects;

public class PairModel {

    private final String mCode;
    private final String mName;
    private boolean isFavorite;

    public PairModel(@NonNull String code, @NonNull String name) {
        mCode = code;
        mName = name;
    }

    public PairModel(@NonNull PairEntity pair) {
        this(pair.getCode(), pair.getName());
        this.isFavorite = pair.isFavorite();
    }

    public String getCode() {
        return mCode;
    }

    public String getName() {
        return mName;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairModel pairModel = (PairModel) o;
        return Objects.equals(mCode, pairModel.mCode) &&
                Objects.equals(mName, pairModel.mName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mCode, mName);
    }
}
