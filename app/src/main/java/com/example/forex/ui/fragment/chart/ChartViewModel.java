package com.example.forex.ui.fragment.chart;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.forex.BuildConfig;
import com.example.forex.application.Logger;
import com.example.forex.architecture.BaseViewModel;
import com.example.forex.socket.ISocketClient;
import com.example.forex.socket.OnSocketClientListener;
import com.example.forex.socket.SocketClient;
import com.example.forex.socket.request.SubscribeTickerRequest;
import com.example.forex.socket.response.BaseEventResponse;
import com.example.forex.socket.response.ticker.TickerChangeEventResponse;

import com.example.forex.ui.fragment.chart.model.CandleModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ChartViewModel extends BaseViewModel {

    private ISocketClient mISocketClient;

    private final MutableLiveData<String> mPairLiveData = new MutableLiveData<>();
    private final MutableLiveData<List<CandleModel>> mCandlesLiveData = new MutableLiveData<>();

    private LifecycleOwner mLifecycleOwner;
    private final LifecycleObserver mLifecycleObserver = (LifecycleEventObserver) (source, event) -> {
        switch (event) {
            case ON_RESUME: {
                openSocket();
                break;
            }
            case ON_PAUSE: {
                closeSocket();
                break;
            }
        }
    };

    @Inject
    ChartViewModel() {
        super();
        initSocket();
    }

    @Override
    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        super.subscribeToLiveData(owner);

        mLifecycleOwner = owner;
        owner.getLifecycle().addObserver(mLifecycleObserver);

        mCandlesLiveData.observe(owner, dateModels -> isShowProgressLiveData.setValue(false));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mLifecycleOwner.getLifecycle().removeObserver(mLifecycleObserver);
        mLifecycleOwner = null;
    }

    MutableLiveData<List<CandleModel>> getCandlesLiveData() {
        return mCandlesLiveData;
    }

    void bindPair(@NonNull String pair) {
        mPairLiveData.setValue(pair);
    }

    private void initSocket() {
        mISocketClient = new SocketClient(BuildConfig.FOREX_HOST);
        mISocketClient.setOnSocketClientListener(new OnSocketClientListener() {
            @Override
            public void onConnect() {
                subscribeTicker();
            }

            @Override
            public void onDisconnect() {
            }

            @Override
            public void onMessage(String message) {
            }

            @Override
            public void onError(Throwable throwable) {
                showError(throwable);
            }

            @Override
            public void onMessageReceived(@NonNull BaseEventResponse response) {
                Logger.debug(String.valueOf(response));

                if (response instanceof TickerChangeEventResponse) {
                    TickerChangeEventResponse response1 = (TickerChangeEventResponse) response;
                    List<CandleModel> entries = new ArrayList<>();
                    String value = mPairLiveData.getValue();
                    assert value != null;
                    entries.add(new CandleModel(value, response1));
                    mCandlesLiveData.setValue(entries);
                }
            }
        });
    }

    private void openSocket() {
        if (mISocketClient != null) {
            mISocketClient.connect();
        }
    }

    private void closeSocket() {
        if (mISocketClient != null) {
            mISocketClient.disconnect();
        }
    }

    private void subscribeTicker() {
        String value = mPairLiveData.getValue();
        assert value != null;
        mISocketClient.invoke(new SubscribeTickerRequest(value));
    }
}