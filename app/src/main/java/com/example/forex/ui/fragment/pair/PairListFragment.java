package com.example.forex.ui.fragment.pair;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuItemCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.forex.R;
import com.example.forex.databinding.FrPairListBinding;
import com.example.forex.rx.RxSearchView;
import com.example.forex.ui.activity.BaseActivity;
import com.example.forex.ui.activity.chart.ChartActivity;
import com.example.forex.ui.fragment.BaseFragment;
import com.example.forex.ui.fragment.pair.adapter.PairListAdapter;
import com.example.forex.ui.fragment.pair.model.PairModel;
import com.example.forex.ui.widget.refresh.RefreshView;
import com.example.forex.ui.widget.search.BaseSearchView;
import com.example.forex.ui.widget.toolbar.IToolbarController;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class PairListFragment extends BaseFragment<FrPairListBinding, PairListViewModel> {

    private RefreshView mRefreshView;
    private PairListAdapter mPairListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fr_pair_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
        mRefreshView = requireView().findViewById(R.id.swipeRefreshView);
        mRefreshView.setEnabled(false);
        initDateList();
    }

    private void initToolbar() {
        BaseActivity activity = (BaseActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(R.string.app_currency_list_title);
    }

    private void initDateList() {
        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        DividerItemDecoration itemDecor = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.item_decor));
        recyclerView.addItemDecoration(itemDecor);
        mPairListAdapter = new PairListAdapter(new PairListAdapter.OnPairListListener() {
            @Override
            public void onCurrencyClick(@NonNull PairModel model) {
                NavController navController = Navigation.findNavController(requireView());
                Bundle bundle = new Bundle();
                bundle.putString(ChartActivity.PAIR, model.getCode());
                navController.navigate(R.id.ac_chart, bundle);
            }

            @Override
            public void onFavoritePairClick(@NonNull PairModel model) {
                if (model.isFavorite())
                    getViewModel().deleteFavorite(model);
                else
                    getViewModel().addFavorite(model);
            }
        });
        recyclerView.setAdapter(mPairListAdapter);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.pair_menu, menu);
        initSearchView(menu);
    }

    private void initSearchView(@NonNull Menu menu) {
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        BaseSearchView searchView = (BaseSearchView) MenuItemCompat.getActionView(searchMenuItem);
        searchView.setQueryHint(getString(R.string.app_pair_list_search_hint));
        Observable<String> obs = RxSearchView.queryTextChangeEvents(searchView)
                .debounce(200, TimeUnit.MILLISECONDS)
                .map(e -> e.queryText().toString());
        getViewModel().bindSearchView(obs);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bind();
    }

    private void bind() {
        getViewModel().subscribeToLiveData(this);
        getViewModel().getCurrencyPairListLiveData().observe(getViewLifecycleOwner(),
                list -> mPairListAdapter.replaceDataSet(list)
        );
    }

    @Override
    protected void onShowProgressView() {
        mRefreshView.show();
    }

    @Override
    protected void onHideProgressView() {
        mRefreshView.hide();
    }
}