package com.example.forex.ui.fragment.chart;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.forex.R;
import com.example.forex.databinding.FrChartBinding;
import com.example.forex.ui.activity.BaseActivity;
import com.example.forex.ui.activity.chart.ChartActivity;
import com.example.forex.ui.fragment.BaseFragment;
import com.example.forex.ui.fragment.chart.model.CandleModel;
import com.example.forex.ui.widget.refresh.RefreshView;
import com.example.forex.ui.widget.toolbar.IToolbarController;

import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;

import java.util.List;

public class ChartFragment extends BaseFragment<FrChartBinding, ChartViewModel> {

    private RefreshView mRefreshView;
    private CandleStickChart mCandleStickChart;

    @Override
    protected int getLayoutId() {
        return R.layout.fr_chart;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
        mRefreshView = requireView().findViewById(R.id.swipeRefreshView);
        mRefreshView.setEnabled(false);
        initCandleChartView();
    }

    private void initCandleChartView() {
        mCandleStickChart = requireView().findViewById(R.id.chartView);
        mCandleStickChart.setBackgroundColor(Color.WHITE);

        mCandleStickChart.getDescription().setEnabled(false);

        mCandleStickChart.setMaxVisibleValueCount(60);
        mCandleStickChart.setPinchZoom(false);
        mCandleStickChart.setDrawGridBackground(false);

        XAxis xAxis = mCandleStickChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        YAxis leftAxis = mCandleStickChart.getAxisLeft();
        leftAxis.setLabelCount(7, false);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawAxisLine(false);

        YAxis rightAxis = mCandleStickChart.getAxisRight();
        rightAxis.setEnabled(false);

        mCandleStickChart.getLegend().setEnabled(false);

        CandleData data = new CandleData();
        data.setValueTextColor(Color.WHITE);

        mCandleStickChart.setData(data);
    }

    private void setData(@NonNull List<CandleModel> entryPoints) {
        addCandleEntry(entryPoints.get(0));
    }

    private void addCandleEntry(@NonNull CandleModel entry) {
        CandleData data = mCandleStickChart.getData();
        if (data != null) {
            ICandleDataSet set = data.getDataSetByIndex(0);
            if (set == null) {
                set = createCandleSet(entry.getPair());
                data.addDataSet(set);
            }

            data.addEntry(new CandleEntry(set.getEntryCount(),
                    entry.getHigh(), entry.getLow(),
                    entry.getOpen(), entry.getClose()),
                    0
            );
            data.notifyDataChanged();

            mCandleStickChart.notifyDataSetChanged();
            mCandleStickChart.setVisibleXRangeMaximum(120);
            mCandleStickChart.moveViewToX(data.getEntryCount());
        }
    }

    private CandleDataSet createCandleSet(@NonNull String label) {
        CandleDataSet set = new CandleDataSet(null, label);
        set.setDrawIcons(false);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setShadowColor(Color.DKGRAY);
        set.setShadowWidth(0.7f);
        set.setDecreasingColor(Color.RED);
        set.setDecreasingPaintStyle(Paint.Style.FILL);
        set.setIncreasingColor(Color.rgb(122, 242, 84));
        set.setIncreasingPaintStyle(Paint.Style.STROKE);
        set.setNeutralColor(Color.BLUE);
        return set;
    }

    private void initToolbar() {
        BaseActivity activity = (BaseActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(R.string.app_chart);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bind();
    }

    private void bind() {
        getViewModel().subscribeToLiveData(getViewLifecycleOwner());
        getViewModel().getCandlesLiveData().observe(getViewLifecycleOwner(), this::setData);
        getViewModel().bindPair(getPair());
    }

    private String getPair() {
        FragmentActivity activity = requireActivity();
        Intent intent = activity.getIntent();
        Bundle arguments = intent.getExtras();
        return (arguments != null)
                ? arguments.getString(ChartActivity.PAIR, ChartActivity.DEFAULT_PAIR)
                : ChartActivity.DEFAULT_PAIR;
    }

    @Override
    protected void onShowProgressView() {
        mRefreshView.show();
    }

    @Override
    protected void onHideProgressView() {
        mRefreshView.hide();
    }
}