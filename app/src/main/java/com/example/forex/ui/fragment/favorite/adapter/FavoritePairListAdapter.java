package com.example.forex.ui.fragment.favorite.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.example.forex.R;
import com.example.forex.databinding.ItemFavoritePairListBinding;
import com.example.forex.ui.binding.BaseRecyclerAdapter;
import com.example.forex.ui.fragment.pair.model.PairModel;

public class FavoritePairListAdapter extends BaseRecyclerAdapter<PairModel, BaseRecyclerAdapter.ViewHolder> {

    private final OnPairListListener mOnPairListListener;

    public FavoritePairListAdapter(@NonNull OnPairListListener listener) {
        super(R.layout.item_favorite_pair_list);
        mOnPairListListener = listener;
    }

    @Override
    protected ViewHolder buildViewHolder(ViewDataBinding binding) {
        return new ViewHolder(binding);
    }

    class ViewHolder extends BaseRecyclerAdapter.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding);
            ((ItemFavoritePairListBinding) binding).setListener(mOnPairListListener);
        }
    }

    public interface OnPairListListener {
        void onCurrencyClick(@NonNull PairModel model);
        void onFavoritePairClick(@NonNull PairModel model);
    }
}
