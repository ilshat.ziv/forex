package com.example.forex.di.dagger;

import com.example.forex.ui.fragment.pair.PairListFragment;
import com.example.forex.ui.fragment.favorite.FavoritePairListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface ExchangeRatesBindingModule {

    @FragmentScope
    @ContributesAndroidInjector
    PairListFragment provideDateListFragment();

    @FragmentScope
    @ContributesAndroidInjector
    FavoritePairListFragment provideFavoritePairListFragment();
}
