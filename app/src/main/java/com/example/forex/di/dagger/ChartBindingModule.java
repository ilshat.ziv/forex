package com.example.forex.di.dagger;

import com.example.forex.ui.fragment.chart.ChartFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface ChartBindingModule {

    @FragmentScope
    @ContributesAndroidInjector
    ChartFragment provideChartFragment();
}
