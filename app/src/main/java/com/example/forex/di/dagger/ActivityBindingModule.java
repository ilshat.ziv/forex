package com.example.forex.di.dagger;

import com.example.forex.ui.activity.chart.ChartActivity;
import com.example.forex.ui.activity.pair.PairActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {
            ExchangeRatesBindingModule.class
    })
    PairActivity contributesExchangeRatesActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {
            ChartBindingModule.class
    })
    ChartActivity contributesChartActivity();
}