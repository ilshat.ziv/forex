package com.example.forex.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.example.forex.application.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import io.reactivex.annotations.NonNull;

public final class FileUtils {

    public static String loadJsonFromAssets(@NonNull Context context, @NonNull String path) {
        String json = null;
        try {
            AssetManager assets = context.getAssets();
            InputStream is = assets.open(path);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            Logger.exception(e);
            return null;
        }
        return json;
    }
}
