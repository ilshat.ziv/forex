package com.example.forex.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    @SuppressLint("SimpleDateFormat")
    public static String formatDataString(long date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(new Date(date));
    }

    public interface Format {
        String EXCHANGE_FORMAT = "dd/MM/yyyy";
    }
}