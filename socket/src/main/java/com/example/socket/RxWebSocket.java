package com.example.socket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import com.example.socket.entities.SocketClosedEvent;
import com.example.socket.entities.SocketClosingEvent;
import com.example.socket.entities.SocketEvent;
import com.example.socket.entities.SocketFailureEvent;
import com.example.socket.entities.SocketMessageEvent;
import com.example.socket.entities.SocketOpenEvent;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

import okhttp3.WebSocket;

public class RxWebSocket {

    private final WebSocketOnSubscribe mWebSocketOnSubscribe;
    private PublishProcessor<SocketEvent> mSocketEventProcessor = PublishProcessor.create();
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private CompositeDisposable mConnectionDisposables = null;
    private WebSocket mWebSocket = null;

    public RxWebSocket(@NonNull String host) {
        mWebSocketOnSubscribe = new WebSocketOnSubscribe(host);
    }

    private Flowable<SocketEvent> getEventSource() {
        return mSocketEventProcessor.onErrorResumeNext(throwable -> {
            mSocketEventProcessor = PublishProcessor.create();
            return mSocketEventProcessor;
        });
    }

    public Flowable<SocketOpenEvent> onOpen() {
        return getEventSource()
                .ofType(SocketOpenEvent.class)
                .doOnEach(new RxWebSocketLogger("onOpen"));
    }

    public Flowable<SocketClosedEvent> onClosed() {
        return getEventSource()
                .ofType(SocketClosedEvent.class)
                .doOnEach(new RxWebSocketLogger("onClosed"));
    }

    public Flowable<SocketClosingEvent> onClosing() {
        return getEventSource()
                .ofType(SocketClosingEvent.class)
                .doOnEach(new RxWebSocketLogger("onClosing"));
    }

    public Flowable<SocketFailureEvent> onFailure() {
        return getEventSource()
                .ofType(SocketFailureEvent.class)
                .doOnEach(new RxWebSocketLogger("onFailure"));
    }

    public Flowable<SocketMessageEvent> onTextMessage() {
        return getEventSource()
                .ofType(SocketMessageEvent.class)
                .filter(SocketMessageEvent::isText)
                .doOnEach(new RxWebSocketLogger("onTextMessage"));
    }

    public Flowable<SocketMessageEvent> onBinaryMessage() {
        return getEventSource()
                .ofType(SocketMessageEvent.class)
                .filter(event -> !event.isText())
                .doOnEach(new RxWebSocketLogger("onBinaryMessage"));
    }

    public synchronized void connect() {
        mConnectionDisposables = new CompositeDisposable();
        Disposable webSocketInstanceDisposable = getEventSource()
                .ofType(SocketOpenEvent.class)
                .firstElement()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(
                        socketOpenEvent -> mWebSocket = socketOpenEvent.getWebSocket(),
                        Throwable::printStackTrace);
        Disposable connectionDisposable = Flowable.create(mWebSocketOnSubscribe, BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(
                        event -> mSocketEventProcessor.onNext(event),
                        Throwable::printStackTrace);
        mConnectionDisposables.add(webSocketInstanceDisposable);
        mConnectionDisposables.add(connectionDisposable);
        mCompositeDisposable.add(connectionDisposable);
    }

    public synchronized Single<Boolean> sendMessage(@NonNull Gson gson, @Nullable Object event) {
        return Single.fromCallable(() -> {
            if (mWebSocket != null) {
                String jsonBody = gson.toJson(event);
                return mWebSocket.send(jsonBody);
            } else {
                throw new RuntimeException("WebSocket not connected!");
            }
        });
    }

    public synchronized Single<Boolean> close() {
        return Single.fromCallable(() -> {
            if (mWebSocket != null) {
                mCompositeDisposable.add(getEventSource()
                        .ofType(SocketClosedEvent.class)
                        .subscribe(event -> {
                            mConnectionDisposables.clear();
                            mCompositeDisposable.clear();
                        }, Throwable::printStackTrace));
                return mWebSocket.close(1000, null);
            } else {
                throw new RuntimeException("WebSocket not connected!");
            }
        }).doOnSuccess(success -> mWebSocket = null);
    }
}
