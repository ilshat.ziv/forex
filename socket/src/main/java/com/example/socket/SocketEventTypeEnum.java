package com.example.socket;

public enum SocketEventTypeEnum {
    OPEN, CLOSING, CLOSED, FAILURE, MESSAGE
}
