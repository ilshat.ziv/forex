package com.example.socket.entities;

import com.example.socket.SocketEventTypeEnum;

public class SocketClosedEvent extends SocketEvent {

    private final int mCode;
    private final String mReason;

    public SocketClosedEvent(int code, String reason) {
        super(SocketEventTypeEnum.CLOSED);
        this.mCode = code;
        this.mReason = reason;
    }

    public int getCode() {
        return mCode;
    }

    public String getReason() {
        return mReason;
    }

    @Override
    public String toString() {
        return "SocketClosedEvent{" +
                "code=" + mCode +
                ", reason='" + mReason + '\'' +
                '}';
    }
}
