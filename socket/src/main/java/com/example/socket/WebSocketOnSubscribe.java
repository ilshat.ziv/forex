package com.example.socket;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import com.example.socket.entities.SocketEvent;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class WebSocketOnSubscribe implements FlowableOnSubscribe<SocketEvent> {

    private final OkHttpClient client;
    private final Request request;

    WebSocketOnSubscribe(@NonNull String url) {
        client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        request = new Request.Builder()
                .url(url)
                .build();
    }

    @Override
    public void subscribe(FlowableEmitter<SocketEvent> emitter) {
        client.newWebSocket(request, new WebSocketEventRouter(emitter));
    }
}

